# Data Visualization with Python: IBM Course

## Introduction

Embark on a visual exploration journey through various datasets with Python as your guiding tool. This repository hosts a bouquet of Jupyter notebooks each blooming with insights and vivid illustrations. The datasets unravel themselves through a variety of plots, charts, and maps made with popular Python libraries such as Pandas, Matplotlib, Seaborn, and Folium.

## Contents

### 1. **[Area Plots, Histograms, and Bar Plots](./DV0101EN-Exercise-Area-Plots-Histograms-and-Bar-Charts.ipynb)**
   - **Estimated Time**: 30 minutes
   - Enlighten your datasets with area plots, histograms, and bar plots to extract and present data insights in a visually appealing manner.
   - **Key Objectives**:
     - Create visually engaging area plots, histograms, and bar charts.
     - Utilize visualizations to unfold the underlying data stories.

### 2. **[Generating Maps with Python](./DV0101EN-Exercise-Generating-Maps-in-Python.ipynb)**
   - **Estimated Time**: 30 minutes
   - Traverse through geospatial data with interactive maps using the Folium library, exploring markers and creating engaging choropleth maps.
   - **Key Objectives**:
     - Navigate and visualize geospatial data with interactive maps.
     - Explore and implement various map types for enriched geographical visualization.

### 3. **[Data Visualization](./DV0101EN-Exercise-Introduction-to-Matplotlib-and-Line-Plots.ipynb)**
   - **Estimated Time**: 30 minutes
   - Unveil the stories hidden within datasets through powerful visual narratives using line plots and other visualization techniques.
   - **Key Objectives**:
     - Utilize Python libraries to create powerful visual representations.
     - Master the art of storytelling with data.

### 4. **[Pie Charts, Box Plots, Scatter Plots, and Bubble Plots](./DV0101EN-Exercise-Pie-Charts-Box-Plots-Scatter-Plots-and-Bubble-Plots.ipynb)**
   - **Estimated Time**: 30 minutes
   - Explore the Matplotlib library further with pie charts, box plots, scatter plots, and bubble charts to deliver an enhanced visual storytelling experience.
   - **Key Objectives**:
     - Delve into the advanced features of the Matplotlib library.
     - Develop various plots and charts for enriched data interpretation.

### 5. **[Waffle Charts, Word Clouds, and Regression Plots](./DV0101EN-Exercise-Waffle-Charts-Word-Clouds-and-Regression-Plots.ipynb)**
   - **Estimated Time**: 30 minutes
   - Dive deep into datasets with Pandas and portray the findings through vibrant waffle charts and expressive word clouds. Also, unveil the relationship between data variables using elegant regression plots made with the Seaborn library.
   - **Key Objectives**:
     - Craft captivating waffle charts and word clouds.
     - Create exquisite regression plots using the Seaborn library.

## Conclusion

This repository is a treasure trove for anyone enthusiastic about learning data visualization with Python. The vivid display of data through various visual mediums ensures a rewarding and insightful learning experience.
